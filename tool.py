"""
https://blog.csdn.net/Yellow_python/article/details/80723272
"""
from time import time, strftime, sleep
END = '\033[0m'


class Color:

    @staticmethod
    def _wrap_colour(colour, args, prints, sep):
        if prints:
            for a in args:print(colour + '{}'.format(a) + END)  # Linux系统会报错
        return colour + sep.join('{}'.format(a) for a in args) + END

    @classmethod
    def background(cls, *args, prints=True, sep=' '):
        return cls._wrap_colour('\033[0;7m', args, prints, sep)

    @classmethod
    def background_yellow(cls, *args, prints=False, sep=' '):
        return cls._wrap_colour('\033[033;7m', args, prints, sep)

    @classmethod
    def blue(cls, *args, prints=True, sep=' '):
        return cls._wrap_colour('\033[94m', args, prints, sep)

    @classmethod
    def cyan(cls, *args, prints=True, sep=' '):
        return cls._wrap_colour('\033[96m', args, prints, sep)

    @classmethod
    def dark_blue(cls, *args, prints=False, sep=' '):
        return cls._wrap_colour('\033[34m', args, prints, sep)

    @classmethod
    def dark_cyan(cls, *args, prints=False, sep=' '):
        return cls._wrap_colour('\033[36m', args, prints, sep)

    @classmethod
    def dark_red(cls, *args, prints=False, sep=' '):
        return cls._wrap_colour('\033[031m', args, prints, sep)

    @classmethod
    def pink(cls, *args, prints=True, sep=' '):
        return cls._wrap_colour('\033[95m', args, prints, sep)

    @classmethod
    def red(cls, *args, prints=True, sep=' '):
        return cls._wrap_colour('\033[91m', args, prints, sep)

    @classmethod
    def yellow(cls, *args, prints=True, sep=' '):
        return cls._wrap_colour('\033[93m', args, prints, sep)

    @classmethod
    def highlight(cls, text, word):
        highlight_word = cls.background_yellow(word)
        len_w = len(word)
        len_t = len(text)
        for i in range(len_t - len_w, -1, -1):
            if text[i: i + len_w] == word:
                text = text[:i] + highlight_word + text[i + len_w:]
        print(text)

    @classmethod
    def hot(cls, value, half=.5):
        return cls.dark_blue(value) if value < half else cls.dark_red(value)

    @classmethod
    def bar(cls, value, maximum=1, label=''):
        length = int(value / maximum * 100)
        if label:
            print(cls.background(label.ljust(length, ' '), prints=False)+cls.hot(value, maximum / 2))
        return '#' * length


class Timer(Color):
    def __init__(self):
        self.t = time()

    def __del__(self):
        if self.__str__():
            print('end time %s' % self.dark_cyan(self.strftime()), 'run time %s' % self.dark_cyan(self))

    def __str__(self):
        t = self.second
        if t < .3:
            return ''
        elif t < 60:
            return '%.2fs' % t
        elif t < 3600:
            return '%.2fmin' % (t / 60)
        else:
            return '%.2fh' % (t / 3600)

    @property
    def second(self) -> float:
        """程序运行秒数"""
        return time() - self.t

    @staticmethod
    def strftime(fm='%Y-%m-%d %H:%M:%S') -> str:
        """当前时间字符串"""
        return strftime(fm)

    @staticmethod
    def sleep(secs=1):
        sleep(secs)

    def write_log(self, *args, **kwargs):
        self.red(args, kwargs)
        with open(self.strftime('%Y-%m-%d') + '.log', 'a', encoding='utf-8') as f:
            for i in args:
                f.write('%r\n' % i)
            for k, v in kwargs.items():
                f.write('%r\n%r\n' % (k, v))
            f.write('\n')


if __name__ == '__main__':
    _t = Timer()
    print(_t.t)
    Timer.blue('blue')
    Timer.cyan('cyan')
    print(_t.dark_blue('dark', 'blue'))
    print(_t.dark_cyan('dark', 'cyan'))
    print(_t.dark_red('dark', 'red'))
    Color.pink('pink')
    Color.red('r', 'e', 'd')
    _t.yellow('yellow')
    _t.background('background', prints=False)
    print(Timer.strftime())
    print(_t.highlight('一片一片又一片', '一片'))
    _t.bar(30, 100, 'boy');_t.bar(.7, 1, 'girl')
    print(_t.bar(50, 100), 50);print(_t.bar(.5, 1), .5)
    print(_t);_t.sleep(1);print(_t)

import pandas as pd, pickle
from os import path

PATH_JIEBA = path.join(path.dirname(__file__), 'dict.txt')
STOP_WORDS = set('''
了 是 在 和 有 他 我 的 也 为 就 这 都 等 着 来 与 要 又 而 一个 之 以 她 去 那 但 把 我们 可 他们 并 自己 或 由 其 给 使 却
这个 它 已经 及 这样 这些 此 们 这种 如果 因为 即 其中 现在 一些 以及 同时 由于 所以 这里 因 曾 呢 但是 该 每 其他 应 吧 虽然
因此 而且 啊 应该 当时 那么 这么 仍 还有 如此 既 或者 然后 有些 那个 关于 于是 不仅 只要 且 另外 而是 还是 此外 这次 如今 就是
并且 从而 其它 尽管 还要 即使 总是 只有 只是 而言 每次 这是 就会 那是'''.strip().split())  # 情感分析时不用
_FLAG_EN2CN = {
    'a': ('形容词', 'adjective'),
    'ad': ('副形词', 'adjective_adverb'),
    'an': ('名形词', 'adjective_noun'),
    'b': ('区别词', '-'),
    'c': ('连词', 'conjunction'),
    'd': ('副词', 'adverb'),
    'e': ('叹词', 'exclamation'),
    'f': ('方位词', 'direction'),
    'i': ('成语', 'idiom'),
    'j': ('简称', 'abbreviation'),
    'l': ('方言', 'localism'),
    'm': ('数词', 'numeral'),
    'mq': ('数量词', 'mq'),
    'n': ('名词', 'noun'),
    'nb': ('抽象名词', 'abstract_noun'),
    'ne': ('经济', 'economics'),
    'nf': ('金融', 'finance'),
    'nh': ('历史', 'history'),
    'nic': ('无机化学', 'inorganic_chemistry'),
    'nit': ('信息技术', 'IT'),
    'nl': ('法律', 'law'),
    'nm': ('医学名词', 'medicine'),
    'noc': ('有机化学', 'organic_chemistry'),
    'npf': ('加工食品', 'processed_food'),
    'nr': ('人名', '-'),
    'nrfg': ('古人名', '-'),
    'nrt': ('音译人名', '-'),
    'ns': ('地名', '-'),
    'nt': ('机构', 'organization'),
    'nw': ('名作', 'masterwork'),
    'nz': ('其他专名', '-'),
    'o': ('拟声词', 'onomatopoeia'),
    'p': ('介词', 'preposition'),
    'q': ('量词', 'quantifier'),
    'r': ('代词', 'pronoun'),
    's': ('处所词', 'position'),
    't': ('时间词', 'time'),
    'u': ('助词', 'auxiliary_word'),
    'uv': ('助动词', 'auxiliary'),
    'v': ('动词', 'verb'),
    'va': ('动形词', 'verb_adjective'),
    'vn': ('名动词', 'verb_noun'),
    'x': ('待定', '-'),
    'y': ('语气词', '-'),
}


# 读 -------------------------------------------------------------------------------------------------------------------


def txt2df(fname=PATH_JIEBA, sep=' ', names=None):
    """默认读取jieba词典"""
    return pd.read_table(fname, sep, names=names, header=None)


def sheet2df(fname, sheet_name=0):
    return pd.read_excel(fname, sheet_name=sheet_name)


def csv2df(fname, names=None):
    return pd.read_csv(fname, names=names, header=None)


def pickle2dt(fname):
    with open(fname, 'rb') as f:
        return pickle.load(f)


def txt2ls(fname) -> list:
    with open(fname, encoding='utf-8') as f:
        return f.read().strip().split('\n')


# 处理 -------------------------------------------------------------------------------------------------------------------


def df2dt(df) -> dict:
    assert df.shape[1] == 2
    return dict(df.values)


def ls2df(ls, columns) -> pd.DataFrame:
    return pd.DataFrame(ls, columns=columns)


# 写 -------------------------------------------------------------------------------------------------------------------


def df2sheet(df, fname):
    fname = fname.replace('.xlsx', '') + '.xlsx'
    df.to_excel(fname, index=False)


def df2sheets(ls_of_df, sheet_names, fname='temporary'):
    fname = fname.replace('.xlsx', '') + '.xlsx'
    excel_writer = pd.ExcelWriter(fname)
    for df, sheet_name in zip(ls_of_df, sheet_names):
        df.to_excel(excel_writer, sheet_name, index=False)
    excel_writer.save()


def df2csv(df, fname):
    df.to_csv(fname.replace('.csv', '') + '.csv', index=False)


def dt2pickle(fname, dt):
    fname = fname.replace('.pickle', '') + '.pickle'
    with open(fname, 'wb') as f:
        pickle.dump(dt, f)


def ls2sheet(ls, columns, fname='frequency'):
    df2sheet(ls2df(ls, columns), fname)


def part_of_speech(n=80):
    """词性统计"""
    word2flag = df2dt(txt2df()[[0, 2]])
    dt = {k: set() for k in word2flag.values()}
    for word, flag in word2flag.items():
        dt[flag].add(word)
    print('cn', 'en', 'flag', 'count', 'e.g.', sep='\t')
    for flag, words in sorted(dt.items()):
        examples = '、'.join(words)
        cn, en = _FLAG_EN2CN[flag]
        print(flag, en, cn, len(words), examples[:examples.find('、', n)], sep='\t')


if __name__ == '__main__':
    part_of_speech()

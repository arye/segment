"""
https://blog.csdn.net/Yellow_python/article/details/99084214
"""
import re

RE_WORD = re.compile(r'\w*[a-z\u4e00-\u9fa5]\w*', re.I)
UDE = re.compile(  # 缺'\
    r'[^-_a-zA-Z\d\u4e00-\u9fa5\s,<.>/?;:"\[{\]}|`~!@#$%^&*()=+，《。》？；：‘’“”【】、·！￥…（）—〔〕]')


def strip(data):
    if isinstance(data, str):
        return data.strip()
    return data


def is_word(word, min_len=2):
    if RE_WORD.fullmatch(word) and len(word) >= min_len:
        return word


def return_string(text) -> str:
    if isinstance(text, str):
        return text.strip()
    return ''


def yield_strings(texts):
    for text in texts:
        text = return_string(text)
        if text:
            yield text


def replace_unicode_decode_error(text) -> str:
    """数据入库前，清洗UnicodeDecodeError"""
    return UDE.sub('', text.replace("'", '"'))


def replace_space(text) -> str:
    """清除连续空白"""
    text = re.sub(r'\s*\n\s*', '\n', text.strip())
    text = re.sub(r'[^\S\n]', ' ', text)
    text = re.sub('(?<![\u4e00-\u9fa5]) (?=[\u4e00-\u9fa5])|(?<=[\u4e00-\u9fa5]) (?![\u4e00-\u9fa5])', '', text)
    return text


def replace_space_resolutely(text, substitution='') -> str:
    return re.sub(r'\s+', substitution, text.strip())


def replace_tag(html, completely=True) -> str:
    """替换HTML标签"""
    # 独立元素
    html = re.sub('<img[^>]*>', '', html)  # 图片
    html = re.sub('<br/?>|<br [^<>]*>|<hr/?>|<hr [^<>]*>', '\n', html)  # 换行、水平线
    html = re.sub('&(nbsp|e[mn]sp|thinsp|zwn?j|#13);', ' ', html)  # 空格
    html = re.sub(r'\\xa0|\\u3000', ' ', html)  # 空格
    html = re.sub(r'<!--[\s\S]*?-->', '', html)  # 注释
    html = re.sub(r'<head>[\s\S]*?</head>', '', html)
    html = re.sub(r'<meta[^<>]*>[\s\S]*?</meta>', '', html)  # 元数据
    html = re.sub(r'<style[^<>]*>[\s\S]*?</style>', '', html)  # 样式
    html = re.sub(r'<script[^<>]*>[\s\S]*?</script>', '', html)  # JavaScript
    html = re.sub(r'<s>[\s\S]*?</s>', '', html)  # 删除线（内容也清除）
    html = re.sub('<input>|<input [^>]*>', '', html)  # 输入框（表单中元素）
    # 行内元素
    html = re.sub('<u>|<u [^>]*>|</u>', '', html)  # 下划线 underline
    html = re.sub('<i>|<i [^>]*>|</i>', '', html)  # 斜体 italic
    html = re.sub('<b>|<b [^>]*>|</b>', '', html)  # 粗体
    html = re.sub('<em>|<em [^>]*>|</em>', '', html)  # 强调 emphasize
    html = re.sub('<strong>|<strong [^>]*>|</strong>', '', html)  # 粗体
    html = re.sub('<mark>|<mark [^>]*>|</mark>', '', html)  # 黄色背景填充标记
    html = re.sub('<font>|<font [^>]*>|</font>', '', html)  # 字体
    html = re.sub('<a>|<a [^>]*>|</a>', '', html)  # 超链接
    html = re.sub('<span>|<span [^>]*>|</span>', '', html)  # span
    # 块级元素
    html = re.sub('<p>|<p [^>]*>|</p>', '\n', html)  # 段落
    html = re.sub('<h[1-6][^>]*>|</h[1-6]>', '\n', html)  # 标题
    html = re.sub('<li>|<li [^>]*>|</li>', '\n', html)  # 列表 list
    html = re.sub('<ol>|<ol [^>]*>|</ol>', '\n', html)  # 有序列表 ordered list
    html = re.sub('<ul>|<ul [^>]*>|</ul>', '\n', html)  # 无序列表 unordered list
    html = re.sub('<pre>|<pre [^>]*>|</pre>', '\n', html)  # 预格化，可保留连续空白符
    html = re.sub('<div>|<div [^>]*>|</div>', '\n', html)  # 分割 division
    html = re.sub('<section[^>]*>|</section>', '\n', html)  # 章节
    html = re.sub('<form>|<form [^>]*>|</form>', '\n', html)  # 表单（用于向服务器传输数据）
    html = re.sub('<o:p>|<o:p [^>]*>|</o:p>', '\n', html)  # OFFICE微软WORD段落
    html = re.sub(r'<td[^>]*>([\s\S]*?)</td>', lambda x: ' %s ' % x.group(1), html)  # 表格
    html = re.sub(r'<tr[^>]*>([\s\S]*?)</tr>', lambda x: '%s\n' % x.group(1), html)  # 表格
    html = re.sub(r'<th[^>]*>([\s\S]*?)</th>', lambda x: '%s\n' % x.group(1), html)  # 表格
    html = re.sub(r'<tbody[^>]*>([\s\S]*?)</tbody>', lambda x: '%s\n' % x.group(1), html)  # 表格
    html = re.sub(r'<table[^>]*>([\s\S]*?)</table>', lambda x: '%s\n' % x.group(1), html)  # 表格
    # 剩余标签
    if completely is True:
        html = re.sub(r'<canvas[^<>]*>[\s\S]*?</canvas>', '', html)  # 画布
        html = re.sub(r'<iframe[^<>]*>[\s\S]*?</iframe>', '', html)  # 内框架
        html = re.sub('<([^<>\u4e00-\u9fa5]|微软雅黑|宋体|仿宋)+>', '', html)
    # 转义字符
    html = html.replace('&lt;', '<').replace('&gt;', '>').replace('&quot;', '"').replace('&amp;', '&')
    return replace_space(html)


def digit_cn2en(text) -> str:
    return text.replace('０', '0').replace('１', '1').replace('２', '2').replace('３', '3').replace(
        '４', '4').replace('５', '5').replace('６', '6').replace('７', '7').replace('８', '8').replace('９', '9')


def replace_punctuation(text) -> str:
    """替换标点（英→中）"""
    text = text.strip().replace('(', '（').replace(')', '）')  # 圆括号
    text = re.sub(r'\[\]|【】|（）|{}|<>|“”|‘’', '', text)  # 清除空括号
    text = re.sub('[;；]+', '；', text)  # 分号
    text = re.sub('[!！]+', '！', text)  # 叹号
    text = re.sub('[?？]+', '？', text)  # 问号
    text = re.sub('[.]{3,}|,{3,}|。{3,}|，{3,}|、{3,}|…+', '…', text)  # 省略号
    text = re.sub('(?<=[\u4e00-\u9fa5]),(?=[\u4e00-\u9fa5])', '，', text)  # 逗号
    text = re.sub('(?<=[\u4e00-\u9fa5])[.](?=[\u4e00-\u9fa5])', '。', text)  # 句号
    text = digit_cn2en(text)
    return text


SEP10 = re.compile('[\n。…；;]+|(?<=[\u4e00-\u9fa5])[.]+(?=[\u4e00-\u9fa5])').split
SEP15 = re.compile('[\n。…；;!！?？]+|(?<=[a-z\u4e00-\u9fa5])[.]+(?=[a-z\u4e00-\u9fa5])', re.I).split
SEP20 = re.compile('[!！?？]+').split
SEP30 = re.compile('[,，:：]+').split
SEP40 = re.compile(r'\W+').split  # 非中英文数字下划线
SEP45 = re.compile('[^a-zA-Z\u4e00-\u9fa5]+').split  # 非中英文
SEP50 = re.compile('[^\u4e00-\u9fa5]+').split  # 非中文


def text2sentence(text):
    for i in SEP10(text.strip()):
        i = i.strip()
        if i:
            yield i


def sentence2clause(sentence):
    for i in SEP20(sentence.strip()):
        i = i.strip()
        if i:
            yield i


def clause2phrase(clause):
    for i in SEP30(clause.strip()):
        i = i.strip()
        if i:
            yield i


def ngram(text):
    for i in SEP40(text.strip()):
        if RE_WORD.fullmatch(i):
            yield i


def text2clause(text):
    for i in SEP15(text):
        i = i.strip()
        if i:
            yield i


def text2phrase(text):
    for sentence in SEP15(text):
        for phrase in SEP30(sentence):
            phrase = phrase.strip()
            if phrase:
                yield phrase


RE_TIME = re.compile('[0-9]+([天年月日时分秒°]|小时|分钟|毫秒|时辰)[0-9天年月日时分秒]*')
RE_YMD = re.compile(
    '((19|20)[0-9]{2}年(0?[1-9]|1[012])月(0?[1-9]|[12][0-9]|3[01])日)|'
    '((19|20)[0-9]{2}-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01]))|'
    '((19|20)[0-9]{2}/(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01]))')
re_md = re.compile('(?<![0-9])(0?[1-9]|1[012])月(0?[1-9]|[12][0-9]|3[01])日')
re_ym = re.compile('(?<![0-9])(19|20)[0-9]{2}年(0?[1-9]|1[012])月')
re_url = re.compile(r'[a-z]+(://|\.)[\w-]+(\.[a-z0-9_-]+)+[0-9a-z!%&()*./:=?_~,@^+#-]*', re.I)
re_email = re.compile('[a-z0-9]+@[a-z0-9_-]+([.][0-9a-z!%&()*./:=?_~,@^+#-]+)+', re.I)
re_ip = re.compile(r'\d+\.\d+\.\d+\.\d+')
re_phone = re.compile('([0-9]{3,4}-|86[+])[0-9]{8,}')  # 电话、邮编、编号等

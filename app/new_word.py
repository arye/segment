"""新词探索"""
import re
from collections import Counter
from segment import clean
from segment.corpus import ls2sheet
from segment.hmm import tk as tk1
from segment.hmm_flag import tk as tk2

xlsx = lambda f: f.replace('.xlsx', '') + '.xlsx'
dictionary = set(tk2.word2flag)
tk1.add_re(re.compile('[a-z0-9_-]', re.I), 'ENUM')
tk2.add_re(re.compile('[a-z0-9_-]', re.I), 'ENUM')
N = 9999


def ngram(texts):
    for text in texts:
        for phrase in clean.ngram(text):
            yield phrase


def bar(c):
    maximum = c[0][1] ** .5
    for w, f in reversed(c):
        tk1.bar(f ** .5, maximum, w)


def new_word(texts, fname='new_word.xlsx'):
    """极速探索新词"""
    c = Counter(
        w for p in ngram(texts) for w in tk1.cut(p)
        if clean.is_word(w) and w not in dictionary).most_common(N)
    ls2sheet(c, ('word', 'freq'), fname)
    bar(c)


def new_word_flag(texts, fname='new_word_flag.xlsx'):
    """探索新词及其词性"""
    c = Counter(
        (w.word, w.flag) for p in ngram(texts) for w in tk2.cut(p)
        if clean.is_word(w.word) and w.word not in dictionary).most_common(N)
    maximum = c[0][1] ** .5
    ls = [(i, j, k, tk2.suggest_frequency(i), tk2.bar(k**.5, maximum)) for (i, j), k in c]
    ls2sheet(ls, ('word', 'flag', 'freq', 'suggest_freq', 'bar'), fname)
    print(*ls, sep='\n')


def new_phrase(texts, fname='new_phrase.xlsx'):
    """探索短语"""
    c = Counter(p for p in ngram(texts)if clean.is_word(p) and p not in dictionary).most_common(N)
    ls2sheet(c, ('phrase', 'freq'), fname)
    bar(c)


def frequency(texts, fname='frequency.xlsx'):
    """词频统计"""
    c = Counter(w for p in ngram(texts)for w in tk1.cut(p)).most_common(N)
    ls2sheet([(w, tk1.get_flag(w), f)for w, f in c], ('word', 'flag', 'freq'), fname)
    bar(c)


def new_word_flag_cyclopedia(texts, n=200):
    """探索新词、词性、百科"""
    from segment.app.cyclopedia.spider import cyclopedia
    from jieba import posseg, enable_paddle
    def jieba_cut(tt):
        """深度学习分词算法"""
        enable_paddle()
        for t in tt:
            for p in clean.SEP50(t):
                for w in posseg.cut(p, use_paddle=True):
                    if clean.is_word(w.word) and (w.word not in dictionary):
                        yield w.word, w.flag
    c = Counter(jieba_cut(texts)).most_common(n)
    ls = [(i, j, k, cyclopedia(i)) for (i, j), k in c]
    ls2sheet(ls, ('word', 'flag', 'freq', 'cyclopedia'), 'new_word_cyclopedia')
 

if __name__ == '__main__':
    from segment.app.data10 import X
    new_word(X)
    new_word_flag(X)
    new_phrase(X)
    frequency(X)
    new_word_flag_cyclopedia(X)

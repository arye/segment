"""关键词抽取"""
from math import log10
from collections import Counter
from os.path import exists
from segment import clean, tk

_PATH = 'tfidf.txt'
DISCARDED_FLAGS = {'c', 'd', 'e', 'm', 'mq', 'o', 'p', 'r', 'u', 'y'}
WEIGHTING_FLAGS = {'j': 2, 'nz': 1.5}  # 加权词性


def cut(text):
    for sentence in clean.ngram(text.strip()):
        for word in tk.cut(sentence):
            if clean.is_word(word) and tk.get_flag(word) not in DISCARDED_FLAGS:
                yield word


class Span:
    """关键词抽取"""
    @staticmethod
    def get_length_of_word(weight, word) -> float:
        """词长权重"""
        return weight - (1 / ((len(word) + 1) ** 2))

    @staticmethod
    def get_flag(weight, word) -> float:
        """词性权重"""
        return weight * WEIGHTING_FLAGS.get(word, 1)

    @staticmethod
    def counter(sentence) -> dict:
        """词跨度 + 词频"""
        words = list(cut(sentence))
        reversed_words = words[::-1]
        le = len(words)
        return {w: 1+(f-1)*((words.index(w)+reversed_words.index(w))/le)for w, f in Counter(words).items()}


class TFIDF(Span):
    def __init__(self):
        self.idf = None
        self.idf_max = None

    def train(self, texts):
        texts = [set(cut(text)) for text in texts]
        le = len(texts)
        words = set(w for t in texts for w in t)
        self.idf = {w: log10(le/(sum((w in t) for t in texts)+1))for w in words}
        self.idf_max = log10(le)
        return self

    def save_and_load(self, texts=None, fname=_PATH):
        fname = fname.replace('.txt', '') + '.txt'
        if exists(fname):
            idf = dict()
            with open(fname, encoding='utf-8') as fr:
                for line in fr.read().strip().split('\n'):
                    word, value = line.split()
                    idf[word] = float(value)
            self.idf = idf
            self.idf_max = max(idf.values())
        else:
            self.train(texts)
            with open(fname, 'w', encoding='utf-8') as fw:
                for k, v in sorted(self.idf.items(), key=lambda x: x[1]):
                    fw.write('%s %f\n' % (k, v))
        return self

    def get_idf(self, weight, word) -> float:
        return weight * self.idf.get(word, self.idf_max)

    def extract(self, text) -> dict:
        return {w: self.get_idf(f, w) for w, f in self.counter(text).items()}

    def extract_top_n(self, text, n=10) -> list:
        return sorted(self.extract(text).items(), key=lambda x: x[1], reverse=True)[:n]


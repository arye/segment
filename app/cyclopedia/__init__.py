"""网络请求程序"""
from time import sleep
from requests import get
from random import choice
from chardet import detect  # 自动检测网页编码
from segment.tool import Timer


class R:
    ua = [
        'Mozilla/5.0(compatible;MSIE9.0;WindowsNT6.1;Trident/5.0;',  # IE9.0
        'Mozilla/4.0(compatible;MSIE8.0;WindowsNT6.0;Trident/4.0)',  # IE8.0
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT6.0)',  # IE7.0
        'Mozilla/4.0(compatible;MSIE6.0;WindowsNT5.1)',  # IE6.0
        'Mozilla/5.0(WindowsNT6.1;rv:2.0.1)Gecko/20100101Firefox/4.0.1',  # Firefox4.0.1–Windows
        'Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11',  # Opera11.11–Windows
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Maxthon2.0)',  # 傲游（Maxthon）
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;TencentTraveler4.0)',  # 腾讯TT
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;360SE)',  # 360浏览器
        'Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;TheWorld)',  # 世界之窗（TheWorld）3.x
    ]
    sleep = 2  # SLEEP >= 1
    timeout = 20
    times = 5
    text = ''

    @staticmethod
    def json():
        return dict()

    @classmethod
    def sleep_randomly(cls):
        sleep(choice(list(range(cls.sleep))) + (0 if int(Timer.strftime('%H')) > 7 else 10))


class Request(Timer):
    def get(self, url, times=R.times):
        R.sleep_randomly()
        sleep(choice(list(range(R.sleep))))  # 随机睡眠
        if times < 0:
            return R()  # 空响应
        try:
            r = get(url, headers={'User-Agent': choice(R.ua)}, timeout=R.timeout)
        except Exception as error:
            self.write_log(url, error=error)  # 写日志
            return self.get(url, times - 1)
        if r.status_code == 200:
            r.encoding = detect(r.content)['encoding']
            return r
        else:
            self.write_log(url, status_code=r.status_code)  # 写日志
            return self.get(url, times - 1)

    def get_text(self, url, encoding=None):
        r = self.get(url)
        r.encoding = encoding or r.encoding
        return r.text


driver = Request()


if __name__ == '__main__':
    for _i in (
        ('https://baike.baidu.com/', '百度百科'),
        ('https://baike.so.com/', '360百科'),
        ('https://baike.sogou.com/', '搜狗百科'),
    ):
        print((_i[1], driver.get_text(_i[0])))


"""
请求词语的百科解释
作用：辅助新词发现和词性标注
"""
from urllib import parse
from re import findall
from random import shuffle
from segment.app.cyclopedia import driver


def cyclopedia_baidu(w):
    """百度百科"""
    prefix = 'https://baike.baidu.com/item/'
    url = prefix + parse.quote(w)
    html = driver.get_text(url)
    description = findall('<meta name="description" content="([^"]+?)">', html)
    # print(w, url, description)
    return ''.join(description).strip()


def cyclopedia_360(w):
    """360百科"""
    prefix = 'https://baike.so.com/doc/search?word='
    url = prefix + parse.quote(w)
    html = driver.get_text(url)
    description = findall('<meta name=description content="([^"]+)"', html)
    print(w, url, description)
    return ''.join(description).strip()


def cyclopedia(w):
    functions = [cyclopedia_baidu, cyclopedia_360]
    shuffle(functions)
    for _ in range(len(functions)):
        f = functions.pop()
        description = f(w)
        if description:
            return description


def ff():
    """术语在线"""
    prefix = 'https://www.termonline.cn/index'


def _cyclopedia(words):
    from segment import tk
    words = [w for w in set(words) if w not in tk.word2freq]
    descriptions = [cyclopedia(w) for w in words]
    for w, d in zip(words, descriptions):
        print(w, tk.suggest_frequency(w)+isinstance(d, str)*2, tk.get_flag(w), d)


if __name__ == '__main__':
    # for _w in ('风投', '发改委', 'win10', '奖补'):
    #     driver.yellow(cyclopedia(_w))
    _cyclopedia('''

    创投

    '''.strip().split())

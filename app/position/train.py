"""https://blog.csdn.net/Yellow_python/article/details/104580509"""
from sklearn.gaussian_process import GaussianProcessRegressor
from matplotlib import pyplot as mp
from seaborn import swarmplot
from segment import lcut  # from jieba import cut


def swarm_plot(x, size=0):
    """蜂群图"""
    s = size if size > 0 else (2400/len(x))**.7  # 根据点数调点大小
    print(len(x));swarmplot(y=x, size=s);mp.show()  # 蜂群图


def get_position(xy, judge=lambda a, b: a == b, v=-1):
    """统计实体在文章出现的位置【排行率[0,1)】"""
    positions = []
    for text, entity in xy:
        words = lcut(text)
        length = len(words)
        for i, word in enumerate(words):
            if judge(word, entity):
                positions.append(i / length)
    if v >= 0:  # visualize: size
        swarm_plot(positions, v)
    return positions


def discretize(positions, n=10, v=0):
    """计算位置密度（数据离散化）"""
    xy = [[i / n, 0] for i in range(n-1, -1, -1)]
    for i in positions:
        for j in xy:
            if i >= j[0]:
                j[1] += 1
                break
    total = len(positions)
    y = [i[1] / total for i in xy]  # [0,1)
    x = [[i[0] + .5/n] for i in xy]  # 右移1/2n
    if v > 0:  # visualize: length of bar
        for i in range(n):
            print('%2d' % (x[i][0]*100), '#'*int(y[i]*v), y[i], sep=' '*3)
    return x, y


def train(x, y):
    """根据位置密度训练位置权重"""
    model = GaussianProcessRegressor().fit(x, y)
    a = [[i / 10] for i in range(11)]
    b = model.predict(a)
    print(a, [round(i, 3) for i in b], sep='\n')
    return model


def ensemble(xy, judge=lambda a, b: a == b):
    """全程可视化"""
    positions = get_position(xy, judge, 0)
    x, y = discretize(positions, 10, 100)
    model = train(x, y)
    w = [[i / 200] for i in range(201)]
    z = model.predict(w)
    mp.scatter(x, y, s=66, color='g')
    mp.scatter(w, z, s=6, color='r')
    mp.show()


if __name__ == '__main__':
    _p=[.02,.03,.05,.91,.05,.06,.01,.06,.02,.13,.1,.27,.95,.02,.06,.13,.82,.89,.34,.2,.95,.02,.13,.96,.03,.05,.23,
    .96,.98,.03,.98,.08,.15,.2,.93,.09,.29,.9,.03,.03,.0,.95,.43,.05,.06,.86,.07,.21,.6,.5,.04,.33,.05,.08,.02,.5,
    .68,.03,.36,.38,.41,.71,.02,.98,.0,.42,.01,.16,.01,.09,.03,.36,.17,.03,.44,.94,.07,.42,.9,.04,.87,.05,.45,.99,
    .03,.04,.08,.9,.11,.95,.06,.26,.06,.87,.04,.04,.09,.09,.91,.95,.06,.07,.09,.95,.98,.1,.84,.03,.14,.07,.03,.17,
    .03,.03,.98,.05,.94,.02,.23,.87,.11,.8,.91,.06,.98,.04,.21,.29,.24,.83,.11,.27,.11,.41,.87,.01,.02,.1,.88,.03,
    .22,.11,.96,.03,.13,.18,.27,.94,.03,.18,.1,.39,.07,.27,.93,.07,.1,.82,.05,.25,.07,.4,.79,.8,.05,.06,.05,.6,.9,
    .3,.1,.93,.04,.99,.06,.05,.88,.92,.02,.16,.94,.03,.03,.1,.22,.26,.9,.02,.05,.03,.03,.98,.04,.06,.6,.04,.04,.5,
    .89,.26,.84,.05,.12,.19,.2,.3,.06,.11,.44,.9,.2,.9,.06,.95,.13,.4,.96,.01,.09,.04,.05,.05,.07,.07,.08,.03,.02,
    .1,.02,.27,.29,.92,.03,.24,.28,.91,.09,.64,.95,.03,.95,.02,.04,.02,.21,.15,.29,.6,.03,.62,.04,.27,.31,.85,.08,
    .02,.99,.01,.01,.86,.04,.83,.03,.99,.01,.02,.4,.59,.66,.56,.98,.05,.5,.89,.03,.96,.98,.02,.97,.01,.03,.68,.05,
    .99,.04,.6,.8,.22,.0,.65,.04,.11,.95,.04,.9,.46,.99,.04,.22,.48,.03,.06,.08,.02,.19,.83,.04,.02,.15,.94]#all<1
    _x, _y = discretize(_p, v=100)
    _w = [[i / 200] for i in range(201)]
    mp.scatter(_x, _y, s=66, color='g')
    mp.scatter(_w, train(_x, _y).predict(_w), s=6, color='r')
    mp.show()

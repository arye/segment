"""计算单词在文章中不同位置的权重"""
from sklearn.gaussian_process import GaussianProcessRegressor
from segment import lcut

# 默认模型：开头>结尾>中间
X = [[0], [.1], [.2], [.3], [.4], [.5], [.6], [.7], [.8], [.9], [1]]
Y = [[1], [.2], [.04], [.02], [.01], [0], [0], [.01], [.03], [.1], [.5]]


class Model:
    """https://yellow520.blog.csdn.net/article/details/106985255"""
    @staticmethod
    def predict(position):
        return (2 * position - 1) ** 8

    def extract(self, text, judge):
        words = lcut(text)
        le = len(words)
        # return [(self.predict(i/le), words[i]) for i in range(le) if judge(words[i])]
        denominator = max(le - 1, 1)
        return [(self.predict(i/denominator), words[i]) for i in range(le) if judge(words[i])]


class GPR(Model):
    """高斯过程回归"""
    def __init__(self, x=X, y=Y):
        self.model = GaussianProcessRegressor()
        self.model.fit(x, y)

    def predict(self, position):
        return self.model.predict([[position]])[0][0]


if __name__ == '__main__':
    print(Model().extract('2020年高考时间7月7~8', lambda x: x.isdigit()))
    print(Model().extract('2020', lambda x: x.isdigit()))
    print(Model().extract('', lambda x: x.isdigit()))
    print(GPR().extract('2020年高考7月7~8日', lambda x: x.isdigit()))

"""
参考：https://blog.csdn.net/Yellow_python/article/details/97677183

新闻9分类

1987 car
1961 education
1910 entertainment
1909 fashion
1996 finance
1906 military
1925 politics
1960 science
1989 sports
总数：17543
测试集切分比例：0.25

模型 | 准确率 | 秒
MultinomialNB | 0.8201 | 0.13
LogisticRegression | 0.8518 | 25.44
DecisionTreeClassifier | 0.7321 | 18.93
AdaBoostClassifier | 0.6979 | 23.44
GradientBoostingClassifier | 0.8381 | 793.17
RandomForestClassifier | 0.8399 | 129.67
SVC | 0.8450 | 492.10

建议：
1. 逻辑回归：准确率高且稳定，速度快；C=10时更准但更慢
2. 朴素贝叶斯：速度极快
3. 随机森林：准确率高但不稳定，速度中
"""
from collections import Counter
from numpy import argmax
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from segment import tk, clean, corpus
from warnings import filterwarnings

filterwarnings('ignore')  # 不打印警告
N = 20000


def clear(text):
    text = clean.re_url.sub('', text)
    text = clean.re_email.sub('', text)
    text = clean.re_ip.sub('', text)
    text = clean.replace_punctuation(text)
    return clean.SEP45(text)


def cut(text):
    for sentence in clear(text):
        for word in tk.cut(sentence):
            if word not in corpus.STOP_WORDS:
                yield word


def clf_word(texts, labels, clf=LogisticRegression(C=2, solver='liblinear'), detail=False):
    """词分类，逻辑回归，存EXCEL"""
    # 向量化
    vectorizer = TfidfVectorizer(tokenizer=cut, max_features=N)
    x = vectorizer.fit_transform(texts)
    # 建模
    clf.fit(x, labels)
    classes = clf.classes_
    print(clf.__class__.__name__, tk.hot(clf.score(x, labels)), *classes)
    # 词分类
    c = Counter(w for t in texts for w in cut(t)).most_common(N)
    if detail is False:
        ls = []
        for word, freq in c:
            flag = tk.get_flag(word)  # 词性
            predict_proba = clf.predict_proba(vectorizer.transform([word]))[0]
            max_index = argmax(predict_proba)
            max_proba = predict_proba[max_index]  # 概率
            label = classes[max_index]  # 类别
            ls.append([freq, flag, word, label, max_proba, tk.bar(max_proba)])
        corpus.ls2sheet(ls, ['freq', 'flag', 'word', 'label', 'probability', 'bar'], 'clf_w_' + clf.__class__.__name__)
    else:
        maximum = c[0][1] ** .5
        ls = []
        for word, freq in c:
            flag = tk.get_flag(word)  # 词性
            predict_proba = clf.predict_proba(vectorizer.transform([word]))[0]  # 概率
            label = classes[argmax(predict_proba)]  # 类别
            ls.append([flag, word, label, *predict_proba, freq, tk.bar(freq ** .5, maximum)])
        corpus.ls2sheet(ls, ['flag', 'word', 'label', *clf.classes_, 'freq', 'bar'], 'detail' + clf.__class__.__name__)


def clf_word_statistics(X, Y, unitary=True):
    """词分类，基于统计，存EXCEL，默认一元多标签分类"""
    length = len(Y)
    X = [set(cut(x)) for x in X]
    ls = []
    if unitary:
        for w, total in Counter(w for x in X for w in x).most_common(N):
            c = Counter(Y[i] for i in range(length) if w in X[i])
            label, frequency = c.most_common()[0]
            probability = frequency / total
            ls.append([total, tk.get_flag(w), w, probability, label])
        corpus.ls2sheet(ls, ['total', 'flag', 'word', 'probability', 'label'], 'clf_word_statistics')
    else:
        labels = sorted(set(i for y in Y for i in y))
        for w, total in Counter(w for x in X for w in x).most_common(N):
            probabilities = Counter(y for i in range(length) if w in X[i] for y in Y[i])
            probabilities = [probabilities[label] / total for label in labels]
            ls.append([total, tk.get_flag(w), w, *probabilities])
        corpus.ls2sheet(ls, ['total', 'flag', 'word', *labels], 'clf_word_statistics')


def model_selection(X, y, test_size=.6):
    """模型选择"""
    from sklearn.naive_bayes import MultinomialNB
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.svm import SVC
    # 数据切分
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
    # 向量化
    vectorizer = TfidfVectorizer(tokenizer=cut, max_features=N)
    X_train = vectorizer.fit_transform(X_train)
    X_test = vectorizer.transform(X_test)
    # 建模
    for clf in [
        MultinomialNB(),
        LogisticRegression(),
        LogisticRegression(C=5),
        LogisticRegression(solver='liblinear'),
        LogisticRegression(C=5, solver='liblinear'),
        RandomForestClassifier(),
        SVC(kernel='linear'),
    ]:
        t0 = tk.second
        clf.fit(X_train, y_train)
        print(
            clf.__class__.__name__,
            '耗时', tk.second - t0,
            '训练分', tk.hot(clf.score(X_train, y_train)),
            '测试分', tk.hot(clf.score(X_test, y_test)),
        )


def clf_text_self(X, y):
    """文本分类错误结果存EXCEL"""
    # 建模
    vec = TfidfVectorizer(tokenizer=cut, max_features=N)
    clf = LogisticRegression(C=2.0)
    XX = vec.fit_transform(X)
    clf.fit(XX, y)
    classes = clf.classes_
    ly = len(y)
    # 概率预测
    proba = clf.predict_proba(XX)
    y_pred = [classes[argmax(i)] for i in proba]
    y01 = [1 if y[i] == y_pred[i] else 0 for i in range(ly)]  # 比较预测值与实际值
    proba = [max(i) for i in proba]
    # 混淆矩阵
    matrix = confusion_matrix(y, y_pred)
    ls_of_df = [corpus.pd.DataFrame(matrix, classes, classes)]
    # 筛选预测错误
    df = corpus.pd.DataFrame({
        'X': X,
        'y': y,
        'y_pred': y_pred,
        'probability': proba,
        'y01': y01,
    })
    df = df[df['y01'] == 0][['X', 'y', 'y_pred', 'probability']]
    ls_of_df.append(df)
    # 存EXCEL
    corpus.df2sheets(ls_of_df, ['matrix', 'self'], 'clf_self.xlsx')


class LR:
    def __init__(self):
        self.vec = TfidfVectorizer(tokenizer=cut, max_features=N)
        self.clf = LogisticRegression(C=2, solver='liblinear')

    def fit(self, X, y):
        X = self.vec.fit_transform(X)
        print('vector time', tk)
        self.clf.fit(X, y)
        print('classifier time', tk)

    def vectorize(self, text):
        """单个文本向量化"""
        return self.vec.transform([text])

    def predict(self, x):
        """单个文本分类"""
        return self.clf.predict(self.vectorize(x))[0]

    def predict_proba(self, x):
        """单个文本分类并返回概率"""
        x = self.vectorize(x)
        proba = self.clf.predict_proba(x)
        y_pred = [self.clf.classes_[argmax(i)] for i in proba]
        proba = [max(i) for i in proba]
        return y_pred, proba

    def classification_report(self, X, y, test_size=.5):
        t0 = tk.second
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
        self.fit(X_train, y_train)
        y_pred = self.clf.predict(self.vec.transform(X_test))
        print(classification_report(y_test, y_pred), '耗时：%f' % (tk.second - t0))


if __name__ == '__main__':
    from segment.app.data10 import X, Y
    model_selection(X, Y)
    LR().classification_report(X, Y)
    clf_word_statistics(X, Y)
    clf_word(X, Y)
    clf_text_self(X, Y)

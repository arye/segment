from collections import Counter
from numpy import argmax
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from segment import tk, clean, corpus
from warnings import filterwarnings

filterwarnings('ignore')  # 不打印警告
N = 10000


def text2phrase(text):
    text = clean.re_url.sub('', text)
    text = clean.re_email.sub('', text)
    text = clean.re_ip.sub('', text)
    text = clean.replace_punctuation(text)
    return clean.SEP45(text)


def ngram(text, n=2):
    for phrase in text2phrase(text):
        words = [w for w in tk.cut(phrase) if w not in corpus.STOP_WORDS]
        for i in range(len(words) + 1 - n):
            word = ''.join(words[i: i + n])
            flag = ' '.join(tk.get_flag(w) for w in words[i: i + n])
            yield (word, flag)


def clf_word(texts, labels, clf=LogisticRegression(C=2, solver='liblinear')):
    """词分类，逻辑回归，存EXCEL"""
    # 向量化
    vectorizer = TfidfVectorizer(tokenizer=ngram, max_features=N)
    x = vectorizer.fit_transform(texts)
    # 建模
    clf.fit(x, labels)
    classes = clf.classes_
    print(clf.__class__.__name__, tk.hot(clf.score(x, labels)), *classes)
    # 词分类
    c = Counter(wf for t in texts for wf in ngram(t)).most_common(N)
    ls = []
    for (word, flag), freq in c:
        predict_proba = clf.predict_proba(vectorizer.transform([word]))[0]  # 概率
        label = classes[argmax(predict_proba)]  # 类别
        ls.append([flag, word, label, *predict_proba, freq])
    corpus.ls2sheet(ls, ['flag', 'word', 'label', *clf.classes_, 'freq'], 'detail' + clf.__class__.__name__)


def clf_word_statistics(X, Y):
    """词分类，基于统计，存EXCEL，默认一元多标签分类"""
    length = len(Y)
    ls = []
    labels = sorted(set(i for y in Y for i in y))
    for (word, flag), total in Counter(w for x in X for w in set(ngram(x))).most_common(N):
        probabilities = Counter(y for i in range(length) if word in X[i] for y in Y[i])
        probabilities = [probabilities[label] / total for label in labels]
        ls.append([total, flag, word, *probabilities])
    corpus.ls2sheet(ls, ['total', 'flag', 'word', *labels], 'clf_word_statistics')


if __name__ == '__main__':
    from segment.app.data10 import X, Y
    clf_word(X, Y)
    clf_word_statistics(X, [[y] for y in Y])

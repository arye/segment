from sklearn.metrics import classification_report, roc_curve
from matplotlib import pyplot as mp


def f1score(y_true, y_pred):
    print(classification_report(y_true, y_pred))


def roc(y_true, y_score):
    """输入某一个类别的真实值和预测概率值，输出ROC曲线"""
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    for i in ('fpr', 'tpr', 'thresholds'):
        print('\033[93m{}\033[0m'.format(i), eval(i), sep='\n')
    mp.figure(figsize=(5, 5))
    mp.scatter(fpr, tpr, color='b', s=99, alpha=.3)
    mp.plot(fpr, tpr, color='b', lw=2)
    mp.plot([0, 1], [0, 1], color='g', lw=2, linestyle='--')
    mp.xlabel('False Positive Rate')  # 正例误判概率
    mp.ylabel('True Positive Rate')  # 正例召回率
    mp.show()


if __name__ == '__main__':
    _y_true = [1, 1, 2, 2, 0]
    _y_pred = [1, 0, 2, 2, 0]
    f1score(_y_true, _y_pred)
    print('-' * 99)
    _y_true = [1, 1, 0, 0, 0]
    _y_score = [.9, .7, .8, .5, 0]
    roc(_y_true, _y_score)

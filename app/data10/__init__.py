def load_data():
    from os import path
    from pandas import read_excel
    fname = path.join(path.dirname(__file__), 'data10.xlsx')
    ay = read_excel(fname).values
    x, y = ay[:, 0], ay[:, 1]
    return x, y


X, Y = load_data()

if __name__ == '__main__':
    from pandas import Series
    from collections import Counter
    print(Counter(Y).most_common())  # 标签分类统计
    print(Series(X).str.len().describe())  # 文本长度概览
